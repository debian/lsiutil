# lsiutil

## See here for legal reasons why it can not be hosted here:

https://github.com/thomaslovell/LSIUtil/issues/1

## Works with

```
$ lspci |grep -i scsi
c1:00.0 Serial Attached SCSI controller: Broadcom / LSI SAS3008 PCI-Express Fusion-MPT SAS-3 (rev 02)
```

## Mail to Broadcom

```
To: opensource@broadcom.com
Subject: lsiutil license
Date: Fri, 08 Mar 2024 11:26:25 +0100

Dear Boredcom collegues

Unfortunately you have this hardware that we bought:
c1:00.0 Serial Attached SCSI controller: Broadcom / LSI SAS3008 PCI-Express Fusion-MPT SAS-3 (rev 02)

But to operate it we need the "lsiutil".
But it is very limited license wise:
https://docs.broadcom.com/doc/12354864

So we can not distribute the binary built from source or have the source legally?
Would it be possible to add it to:
https://github.com/Broadcom?q=&type=all&language=&sort=

Any license compatible with Debian DFSG is welcome to me, and would
allow me to put it into official Debian so everyone can just install
it with "apt install lsiutil"

I already have official packaging and debian/ at http://sid.ethz.ch/debian/lsiutil/

Kind regards,
me
```
